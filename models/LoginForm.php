<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property-read User|null $user
 *
 */
class LoginForm extends Model
{
    public $nombreUsuario;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['nombreUsuario', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'nombreUsuario' => 'Nombre de usuario',
            'password' => 'Contraseña',
            'rememberMe' => 'Recordarme',
        ];
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Nombre o contraseña incorrecto.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
{
    if ($this->validate()) {
        // Aquí, verificamos si el usuario existe en la base de datos
        $user = Usuarios::findByUsername($this->nombreUsuario);
        
        if ($user !== null) {
            // Si el usuario existe, intentamos iniciar sesión
            return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
        } else {
            // Si el usuario no existe, muestra un mensaje de error
            $this->addError('nombreUsuario', 'El nombre de usuario no existe.');
        }
    }
    return false;
}

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
{
    if ($this->_user === false) {
        $this->_user = Usuarios::findByUsername($this->nombreUsuario);
    }

    return $this->_user;
}
}
