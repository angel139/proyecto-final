<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $codigoPartido
 * @property string|null $resultado
 * @property string|null $fecha
 * @property int|null $codigoAnalista
 * @property int|null $codigoTorneo
 *
 * @property Analistas $codigoAnalista0
 * @property Torneos $codigoTorneo0
 * @property Juegan[] $juegans
 * @property Campeones[] $nombres
 * @property Tienen[] $tienens
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['codigoAnalista', 'codigoTorneo'], 'integer'],
            [['resultado'], 'string', 'max' => 10],
            [['codigoAnalista'], 'exist', 'skipOnError' => true, 'targetClass' => Analistas::class, 'targetAttribute' => ['codigoAnalista' => 'codigoAnalista']],
            [['codigoTorneo'], 'exist', 'skipOnError' => true, 'targetClass' => Torneos::class, 'targetAttribute' => ['codigoTorneo' => 'codigoTorneo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoPartido' => 'Codigo Partido',
            'resultado' => 'Resultado',
            'fecha' => 'Fecha',
            'codigoAnalista' => 'Codigo Analista',
            'codigoTorneo' => 'Codigo Torneo',
        ];
    }

    /**
     * Gets query for [[CodigoAnalista0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnalista0()
    {
        return $this->hasOne(Analistas::class, ['codigoAnalista' => 'codigoAnalista']);
    }

    /**
     * Gets query for [[CodigoTorneo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoTorneo0()
    {
        return $this->hasOne(Torneos::class, ['codigoTorneo' => 'codigoTorneo']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::class, ['codigoPartido' => 'codigoPartido']);
    }

    /**
     * Gets query for [[Nombres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombres()
    {
        return $this->hasMany(Campeones::class, ['nombre' => 'nombre'])->viaTable('tienen', ['codigoPartido' => 'codigoPartido']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::class, ['codigoPartido' => 'codigoPartido']);
    }
}
