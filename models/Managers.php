<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "managers".
 *
 * @property int $codigoManager
 * @property string|null $correoElectronico
 * @property string|null $nombre
 * @property string|null $telefono
 * @property float|null $sueldo
 *
 * @property Patrocinadores[] $patrocinadores
 * @property Redessociales[] $redessociales
 */
class Managers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'managers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sueldo'], 'number'],
            [['correoElectronico'], 'string', 'max' => 30],
            [['nombre', 'telefono'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoManager' => 'Codigo Manager',
            'correoElectronico' => 'Correo Electronico',
            'nombre' => 'Nombre',
            'telefono' => 'Telefono',
            'sueldo' => 'Sueldo',
        ];
    }

    /**
     * Gets query for [[Patrocinadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinadores()
    {
        return $this->hasMany(Patrocinadores::class, ['codigoManager' => 'codigoManager']);
    }

    /**
     * Gets query for [[Redessociales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRedessociales()
    {
        return $this->hasMany(Redessociales::class, ['codigoManager' => 'codigoManager']);
    }
}
