<?php

namespace app\models;

use Yii;
use yii\validators\RegularExpressionValidator;

/**
 * This is the model class for table "jugadores".
 *
 * @property string $nombreJuego
 * @property string|null $nombre
 * @property string|null $telefono
 * @property string|null $fecha_alta
 * @property string|null $fecha_baja
 * @property float|null $sueldo
 * @property string|null $posicion
 * @property float|null $indiceVictoria
 * @property int|null $totalPartidos
 *
 * @property Entrenadores[] $codigos
 * @property Entrenan[] $entrenans
 * @property Juegan[] $juegans
 * @property Campeones[] $nombres
 * @property Usan[] $usans
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreJuego'], 'required'],
            [['fecha_alta', 'fecha_baja'], 'safe'],
            [[ 'indiceVictoria'], 'integer'],
            [['sueldo'], 'integer', 'min' => 0],
            [['totalPartidos'], 'integer'],
            [['nombreJuego', 'nombre', 'telefono'], 'string', 'max' => 15],
            ['nombre', 'match', 'pattern' => '/^[^0-9]*$/'],
            ['telefono', 'match','pattern' => '/^[^a-zA-Z]*$/'],
            [['posicion'], 'string', 'max' => 8],
            [['nombreJuego'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombreJuego' => 'Nombre Juego',
            'nombre' => 'Nombre',
            'telefono' => 'Teléfono',
            'fecha_alta' => 'Fecha Alta',
            'fecha_baja' => 'Fecha Baja',
            'sueldo' => 'Sueldo',
            'posicion' => 'Posición',
            'indiceVictoria' => 'Indice Victoria',
            'totalPartidos' => 'Total Partidos',
        ];
    }

    /**
     * Gets query for [[Codigos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigos()
    {
        return $this->hasMany(Entrenadores::class, ['codigo' => 'codigo'])->viaTable('entrenan', ['nombreJuego' => 'nombreJuego']);
    }

    /**
     * Gets query for [[Entrenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenans()
    {
        return $this->hasMany(Entrenan::class, ['nombreJuego' => 'nombreJuego']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::class, ['nombreJuego' => 'nombreJuego']);
    }

    /**
     * Gets query for [[Nombres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombres()
    {
        return $this->hasMany(Campeones::class, ['nombre' => 'nombre'])->viaTable('usan', ['nombreJuego' => 'nombreJuego']);
    }

    /**
     * Gets query for [[Usans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsans()
    {
        return $this->hasMany(Usan::class, ['nombreJuego' => 'nombreJuego']);
    }
    
}
