<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property int|null $codigoPartido
 * @property string|null $nombre
 * @property int|null $asesinatos
 * @property int|null $asistencias
 * @property int|null $muertes
 *
 * @property Partidos $codigoPartido0
 * @property Campeones $nombre0
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoPartido', 'asesinatos', 'asistencias', 'muertes'], 'integer'],
            [['nombre'], 'string', 'max' => 15],
            [['codigoPartido', 'nombre'], 'unique', 'targetAttribute' => ['codigoPartido', 'nombre']],
            [['nombre'], 'exist', 'skipOnError' => true, 'targetClass' => Campeones::class, 'targetAttribute' => ['nombre' => 'nombre']],
            [['codigoPartido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::class, 'targetAttribute' => ['codigoPartido' => 'codigoPartido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigoPartido' => 'Codigo Partido',
            'nombre' => 'Nombre',
            'asesinatos' => 'Asesinatos',
            'asistencias' => 'Asistencias',
            'muertes' => 'Muertes',
        ];
    }

    /**
     * Gets query for [[CodigoPartido0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido0()
    {
        return $this->hasOne(Partidos::class, ['codigoPartido' => 'codigoPartido']);
    }

    /**
     * Gets query for [[Nombre0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombre0()
    {
        return $this->hasOne(Campeones::class, ['nombre' => 'nombre']);
    }   
    public function beforeSave($insert)
{
    if (parent::beforeSave($insert)) {
        // Obtiene el nombre del campeón asociado a este registro
        $nombreCampeon = $this->nombre;

      

        // Actualiza las medias en el registro de Campeones correspondiente
        $campeon = Campeones::findOne(['nombre' => $nombreCampeon]);
        if ($campeon !== null) {
            $campeon->asesinatos = Tienen::find()
            ->where(['nombre' => $nombreCampeon])
            ->average('asesinatos');;
            $campeon->muertes = Tienen::find()
            ->where(['nombre' => $nombreCampeon])
            ->average('muertes');;
            $campeon->asistencias = Tienen::find()
            ->where(['nombre' => $nombreCampeon])
            ->average('asistencias');;
            $campeon->save();
        }

        return true;
    }

    return false;
}

}
