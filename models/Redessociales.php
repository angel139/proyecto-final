<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "redessociales".
 *
 * @property int $codigoRed
 * @property string|null $plataforma
 * @property int|null $seguidores
 * @property string|null $direccion
 * @property int|null $codigoManagers
 *
 * @property Managers $codigoManagers0
 */
class Redessociales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'redessociales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoRed'], 'required'],
            [['codigoRed', 'seguidores', 'codigoManager'], 'integer'],
            [['plataforma', 'direccion'], 'string', 'max' => 20],
            [['codigoRed'], 'unique'],
            [['codigoManager'], 'exist', 'skipOnError' => true, 'targetClass' => Managers::class, 'targetAttribute' => ['codigoManager' => 'codigoManager']],
            [['seguidores'], 'integer', 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoRed' => 'Código Red',
            'plataforma' => 'Plataforma',
            'seguidores' => 'Seguidores',
            'direccion' => 'Dirección',
            'codigoManager' => 'Código Manager',
        ];
    }

    /**
     * Gets query for [[CodigoManagers0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoManager0()
    {
        return $this->hasOne(Managers::class, ['codigoManager' => 'codigoManager']);
    }
}
