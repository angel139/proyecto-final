<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string|null $nombrePatrocinadores
 * @property string|null $telefono
 *
 * @property Patrocinadores $nombrePatrocinadores0
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombrePatrocinadores', 'telefono'], 'string', 'max' => 15],
            [['nombrePatrocinadores', 'telefono'], 'unique', 'targetAttribute' => ['nombrePatrocinadores', 'telefono']],
            [['nombrePatrocinadores'], 'exist', 'skipOnError' => true, 'targetClass' => Patrocinadores::class, 'targetAttribute' => ['nombrePatrocinadores' => 'nombre']],
            ['telefono', 'match','pattern' => '/^[^a-zA-Z]*$/']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombrePatrocinadores' => 'Nombre Patrocinadores',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[NombrePatrocinadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePatrocinadores0()
    {
        return $this->hasOne(Patrocinadores::class, ['nombre' => 'nombrePatrocinadores']);
    }
}
