<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $codigoEntrenador
 * @property float|null $sueldo
 * @property string|null $nombre
 * @property string|null $mote
 * @property string $fecha_alta
 * @property string|null $fecha_baja
 * @property string|null $funcion
 * @property int|null $codigoAnalista
 *
 * @property Analistas $codigoAnalista0
 * @property Entrenan[] $entrenans
 * @property Jugadores[] $nombreJuegos
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sueldo'], 'number'],
            [['fecha_alta'], 'required'],
            [['fecha_alta', 'fecha_baja'], 'safe'],
            [['codigoAnalista'], 'integer'],
            [['nombre', 'mote', 'funcion'], 'string', 'max' => 15],
            [['codigoAnalista'], 'exist', 'skipOnError' => true, 'targetClass' => Analistas::class, 'targetAttribute' => ['codigoAnalista' => 'codigoAnalista']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoEntrenador' => 'Codigo Entrenador',
            'sueldo' => 'Sueldo',
            'nombre' => 'Nombre',
            'mote' => 'Mote',
            'fecha_alta' => 'Fecha Alta',
            'fecha_baja' => 'Fecha Baja',
            'funcion' => 'Funcion',
            'codigoAnalista' => 'Codigo Analista',
        ];
    }

    /**
     * Gets query for [[CodigoAnalista0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAnalista0()
    {
        return $this->hasOne(Analistas::class, ['codigoAnalista' => 'codigoAnalista']);
    }

    /**
     * Gets query for [[Entrenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenans()
    {
        return $this->hasMany(Entrenan::class, ['codigoEntrenador' => 'codigoEntrenador']);
    }

    /**
     * Gets query for [[NombreJuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreJuegos()
    {
        return $this->hasMany(Jugadores::class, ['nombreJuego' => 'nombreJuego'])->viaTable('entrenan', ['codigoEntrenador' => 'codigoEntrenador']);
    }
}
