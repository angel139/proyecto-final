<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $id
 * @property string|null $nombreJuego
 * @property int|null $codigoPartido
 *
 * @property Partidos $codigoPartido0
 * @property Jugadores $nombreJuego0
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoPartido'], 'integer'],
            [['nombreJuego'], 'string', 'max' => 15],
            [['nombreJuego'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['nombreJuego' => 'nombreJuego']],
            [['codigoPartido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::class, 'targetAttribute' => ['codigoPartido' => 'codigoPartido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreJuego' => 'Nombre Juego',
            'codigoPartido' => 'Codigo Partido',
        ];
    }

    /**
     * Gets query for [[CodigoPartido0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartido0()
    {
        return $this->hasOne(Partidos::class, ['codigoPartido' => 'codigoPartido']);
    }

    /**
     * Gets query for [[NombreJuego0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreJuego0()
    {
        return $this->hasOne(Jugadores::class, ['nombreJuego' => 'nombreJuego']);
    }
}
