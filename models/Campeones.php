<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campeones".
 *
 * @property string $nombre
 * @property int|null $asesinatos
 * @property int|null $asistencias
 * @property int|null $muertes
 *
 * @property Partidos[] $codigoPartidos
 * @property Jugadores[] $nombreJuegos
 * @property Tienen[] $tienens
 * @property Usan[] $usans
 */
class Campeones extends \yii\db\ActiveRecord
{



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campeones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['asesinatos', 'asistencias', 'muertes'], 'number'],
            [['nombre'], 'string', 'max' => 15],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'asesinatos' => 'Asesinatos',
            'asistencias' => 'Asistencias',
            'muertes' => 'Muertes',
        ];
    }

    /**
     * Gets query for [[CodigoPartidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPartidos()
    {
        return $this->hasMany(Partidos::class, ['codigo' => 'codigoPartidos'])->viaTable('tienen', ['nombre' => 'nombre']);
    }

    /**
     * Gets query for [[NombreJuegos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreJuegos()
    {
        return $this->hasMany(Jugadores::class, ['nombreJuego' => 'nombreJuego'])->viaTable('usan', ['nombre' => 'nombre']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::class, ['nombre' => 'nombre']);
    }

    /**
     * Gets query for [[Usans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsans()
    {
        return $this->hasMany(Usan::class, ['nombre' => 'nombre']);
    }
    
}
