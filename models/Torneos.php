<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "torneos".
 *
 * @property int $codigoTorneo
 * @property string|null $nombre
 * @property string|null $fechaInicio
 * @property string|null $fechaFinal
 * @property float|null $premioRecibido
 * @property string|null $ganador
 * @property string|null $resultado
 *
 * @property Partidos[] $partidos
 */
class Torneos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'torneos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechaInicio', 'fechaFinal'], 'safe'],
            [['premioRecibido'], 'number'],
            [['nombre', 'resultado'], 'string', 'max' => 15],
            [['ganador'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoTorneo' => 'Codigo Torneo',
            'nombre' => 'Nombre',
            'fechaInicio' => 'Fecha Inicio',
            'fechaFinal' => 'Fecha Final',
            'premioRecibido' => 'Premio Recibido',
            'ganador' => 'Ganador',
            'resultado' => 'Resultado',
        ];
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::class, ['codigoTorneo' => 'codigoTorneo']);
    }
}
