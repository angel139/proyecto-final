<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mensajes".
 *
 * @property int $id
 * @property string $destinatario
 * @property string $mensaje
 * @property string $fecha
 *
 * @property Usuarios $destinatario0
 */
class Mensajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mensajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['destinatario', 'mensaje'], 'required'],
            [['fecha'], 'safe'],
            [['destinatario', 'mensaje'], 'string', 'max' => 255],
            [['destinatario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::class, 'targetAttribute' => ['destinatario' => 'nombreUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'destinatario' => 'Destinatario',
            'mensaje' => 'Mensaje',
            'fecha' => 'fecha',
        ];
    }

    /**
     * Gets query for [[Destinatario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinatario()
    {
        return $this->hasOne(Usuarios::class, ['nombreUsuario' => 'destinatario']);
    }
    public function beforeSave($insert)
{
    if ($insert) {
        // Solo establecer la fecha si es una inserción, no una actualización
        $this->fecha = date('Y-m-d H:i:s'); // Formato 'Y-m-d H:i:s' para DATETIME en MySQL
    }
    
    return parent::beforeSave($insert);
}
}
