<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usan".
 *
 * @property int $id
 * @property string|null $nombreJuego
 * @property string|null $nombre
 *
 * @property Campeones $nombre0
 * @property Jugadores $nombreJuego0
 */
class Usan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreJuego', 'nombre'], 'string', 'max' => 15],
            [['nombreJuego', 'nombre'], 'unique', 'targetAttribute' => ['nombreJuego', 'nombre']],
            [['nombre'], 'exist', 'skipOnError' => true, 'targetClass' => Campeones::class, 'targetAttribute' => ['nombre' => 'nombre']],
            [['nombreJuego'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['nombreJuego' => 'nombreJuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreJuego' => 'Nombre Juego',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Nombre0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombre0()
    {
        return $this->hasOne(Campeones::class, ['nombre' => 'nombre']);
    }

    /**
     * Gets query for [[NombreJuego0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreJuego0()
    {
        return $this->hasOne(Jugadores::class, ['nombreJuego' => 'nombreJuego']);
    }
}
