<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $nombreUsuario
 * @property string $password
 * @property string $rol
 *
 * @property Mensajes $mensajes
 */
class Usuarios extends ActiveRecord implements IdentityInterface
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreUsuario', 'password'], 'required'],
            [['nombreUsuario'], 'unique'],
            [['rol'], 'string'],
            [['nombreUsuario', 'password'], 'string', 'max' => 50],
           
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreUsuario' => 'Nombre Usuario',
            'password' => 'password',
            'rol' => 'Rol',
            'tokenAcceso' => 'Token Acceso',
        ];
    }

    /**
     * Gets query for [[Mensajes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMensajes()
    {
        return $this->hasOne(Mensajes::class, ['id' => 'id']);
    }
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // Si no planeas utilizar tokens de acceso, puedes devolver null directamente
        return null;
    }

    // Implementación de getId
    public function getId()
    {
        return $this->id;
    }

    // Implementación de getAuthKey
    public function getAuthKey()
    {
        return $this->tokenAcceso;
    }

    // Implementación de validateAuthKey
    public function validateAuthKey($tokenAcceso)
    {
        return $this->tokenAcceso === $tokenAcceso;
    }

    public function validateUsername()
{
   
    $user = Usuarios::findByUsername($this->nombreUsuario);
    
    if (!$user) {
        $this->addError('nombreUsuario', 'Nombre de usuario no válido.');
    }
}

    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public static function findByUsername($username)
{
    return static::findOne(['nombreUsuario' => $username]);
}
public function beforeSave($insert)
{
    if (parent::beforeSave($insert)) {
        if ($this->isNewRecord) { // Comprueba si es una inserción (nuevo usuario)
            $this->rol = 'jugador'; // Establece el rol por defecto
        }

        return true;
    }
    return false;
}
public function beforeDelete()
{
    if (parent::beforeDelete()) {
        // Buscar y eliminar todos los mensajes dirigidos a este usuario
        Mensajes::deleteAll(['destinatario' => $this->id]);
        return true;
    }
    return false;
}
    
}
