<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "analistas".
 *
 * @property int $codigoAnalista
 * @property string|null $nombre
 * @property float|null $sueldo
 *
 * @property Entrenadores[] $entrenadores
 * @property Partidos[] $partidos
 */
class Analistas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'analistas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sueldo'], 'number'],
            [['nombre'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoAnalista' => 'Codigo Analista',
            'nombre' => 'Nombre',
            'sueldo' => 'Sueldo',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::class, ['codigoAnalista' => 'codigoAnalista']);
    }

    /**
     * Gets query for [[Partidos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPartidos()
    {
        return $this->hasMany(Partidos::class, ['codigoAnalista' => 'codigoAnalista']);
    }
}
