<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenan".
 *
 * @property int $id
 * @property string|null $nombreJuego
 * @property int|null $codigoEntrenador
 *
 * @property Entrenadores $codigoEntrenador0
 * @property Jugadores $nombreJuego0
 */
class Entrenan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigoEntrenador'], 'integer'],
            [['nombreJuego'], 'string', 'max' => 15],
            [['nombreJuego', 'codigoEntrenador'], 'unique', 'targetAttribute' => ['nombreJuego', 'codigoEntrenador']],
            [['codigoEntrenador'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::class, 'targetAttribute' => ['codigoEntrenador' => 'codigoEntrenador']],
            [['nombreJuego'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['nombreJuego' => 'nombreJuego']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreJuego' => 'Nombre Juego',
            'codigoEntrenador' => 'Codigo Entrenador',
        ];
    }

    /**
     * Gets query for [[CodigoEntrenador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEntrenador0()
    {
        return $this->hasOne(Entrenadores::class, ['codigoEntrenador' => 'codigoEntrenador']);
    }

    /**
     * Gets query for [[NombreJuego0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreJuego0()
    {
        return $this->hasOne(Jugadores::class, ['nombreJuego' => 'nombreJuego']);
    }
}
