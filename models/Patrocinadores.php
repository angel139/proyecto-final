<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinadores".
 *
 * @property string $nombre
 * @property float|null $pagoMensual
 * @property string|null $fechaInicio
 * @property string|null $fechaFinal
 * @property string|null $correoElectronico
 * @property int|null $codigoManager
 *
 * @property Managers $codigoManager0
 * @property Telefonos[] $telefonos
 */
class Patrocinadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['pagoMensual'], 'integer', 'min' => 0],
            [['fechaInicio', 'fechaFinal'], 'safe'],
            [['codigoManager'], 'integer'],
            [['nombre'], 'string', 'max' => 15],
            [['correoElectronico'], 'string', 'max' => 30],
            [['nombre'], 'unique'],
            [['codigoManager'], 'exist', 'skipOnError' => true, 'targetClass' => Managers::class, 'targetAttribute' => ['codigoManager' => 'codigoManager']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'pagoMensual' => 'Pago Mensual',
            'fechaInicio' => 'Fecha Inicio',
            'fechaFinal' => 'Fecha Final',
            'correoElectronico' => 'Correo Electrónico',
            'codigoManager' => 'Código Manager',
        ];
    }

    /**
     * Gets query for [[CodigoManager0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoManager0()
    {
        return $this->hasOne(Managers::class, ['codigoManager' => 'codigoManager']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['nombrePatrocinadores' => 'nombre']);
    }
}
