<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Torneos $model */

$this->title = $model->codigoTorneo;
$this->params['breadcrumbs'][] = ['label' => 'Torneos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="torneos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoTorneo' => $model->codigoTorneo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoTorneo' => $model->codigoTorneo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoTorneo',
            'nombre',
            'fechaInicio',
            'fechaFinal',
            'premioRecibido',
            'ganador',
            'resultado',
        ],
    ]) ?>

</div>
