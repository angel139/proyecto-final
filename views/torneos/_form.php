<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/** @var yii\web\View $this */
/** @var app\models\Torneos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="torneos-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechaInicio')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('dd/mm/yyyy', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'yyyy/mm/dd',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'fechaFinal')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('dd/mm/yyyy', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'yyyy/mm/dd',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'premioRecibido')->textInput() ?>

    <?= $form->field($model, 'ganador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'resultado')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
