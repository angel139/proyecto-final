<?php

use app\models\Torneos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Torneos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="torneos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Torneos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            
            'nombre',
            'fechaInicio',
            'fechaFinal',
            'premioRecibido',
            //'ganador',
            //'resultado',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Torneos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoTorneo' => $model->codigoTorneo]);
                 }
            ],
        ],
    ]); ?>


</div>
