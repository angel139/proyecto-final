<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Juegan $model */

$this->title = 'Update Juegan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Juegans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="juegan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1'=>$data1
    ]) ?>

</div>
