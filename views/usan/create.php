<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Usan $model */

$this->title = 'Crear Usan';
$this->params['breadcrumbs'][] = ['label' => 'Usan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
            'data1' => $data1
    ]) ?>

</div>
