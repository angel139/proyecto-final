<?php
use yii\grid\GridView;
/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Finanzas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="finanzas-index">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'category',
            [
                'label' => 'Valor',
                'value' => function ($model) {
                        return $model['value'] . ' €';
                    },
            ],
        ],
    ]) ?>


    <!-- Muestra los torneos del mes actual -->
    <h2>Torneos mensuales</h2>


    <table class="table">
        <tbody>
            <tr>
                <td>
                    <?= \yii\widgets\ListView::widget([
                        'dataProvider' => new \yii\data\ArrayDataProvider([
                            'allModels' => $torneosMensuales,
                        ]),
                        'itemView' => '_torneo',
                        // Nombre de la vista parcial para cada torneo
                        'summary' => '', // Oculta el resumen de ListView
                    ]) ?>
                </td>
            </tr>
        </tbody>
    </table>
    <!-- Muestra el total de ganancias mensuales -->
    <h2>Total ganancias mensuales por torneos</h2>
    <table class="table">
        <tbody>
            <tr>
                <td>Total:</td>
                <td>
                    <?= $totalGananciasMensuales ?>€
                </td>
            </tr>
        </tbody>
    </table>
</div>