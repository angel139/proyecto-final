<?php
use yii\helpers\Html;

// $model representa un modelo de torneo
?>

<div class="torneo-item">
    <h4><?= Html::encode($model->nombre) ?></h4>
    <p>Premio: <?= $model->premioRecibido ?></p>
</div>