<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ver Mensajes';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="mensajes-ver">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($dataProvider->totalCount === 0) : ?>
        <p>No tienes mensajes.</p>
    <?php else : ?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => 'itemMensaje', // Vista parcial para mostrar cada mensaje
        ]) ?>
    <?php endif; ?>

</div>