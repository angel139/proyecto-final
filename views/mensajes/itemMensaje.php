<?php
use yii\helpers\Html;
?>
<div class="mensaje-box">
    <div class="row">
        <div class="col-md-9"> 
            <div class="mensaje-header">
                Fecha: <?= Yii::$app->formatter->asDatetime($model->fecha, 'php:H:i:s d-m-Y') ?>
            </div>
            <div class="mensaje-body">
                <?= $model->mensaje ?>
            </div>
        </div>
        <div class="col-md-3"> 
            <?= Html::button('Marcar como leído', [
                'class' => 'btn btn-sm btn-danger custom-button', 
                'onclick' => 'marcarLeido(' . $model->id . ')'
            ]) ?>
        </div>
    </div>
</div>