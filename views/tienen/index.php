<?php

use app\models\Tienen;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Tienen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tienen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Tienen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            
            'codigoPartido',
            'nombre',
            'asesinatos',
            'asistencias',
            'muertes',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Tienen $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
