<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Tienen $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tienen-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'codigoPartido')->widget(Select2::classname(), [
    'data' => $data1,
    'options' => ['placeholder' => 'Elige un partido'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>
    <?= $form->field($model, 'nombre')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Select a state ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

        

    <?= $form->field($model, 'asesinatos')->textInput() ?>

    <?= $form->field($model, 'asistencias')->textInput() ?>

    <?= $form->field($model, 'muertes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
