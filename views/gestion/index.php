<div class="container text-center mt-5">
    <h2>Menú de administrador</h2>
    <div class="row mt-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Usuarios</h5>
                    <p class="card-text">Administra los usuarios.</p>
                    <a href="<?= $usuariosCrudUrl ?>" class="btn btn-danger">Gestión de usuarios</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">CRUD de Mensajes</h5>
                    <p class="card-text">Administra los mensajes de la aplicación.</p>
                    <a href="<?= $mensajesCrudUrl ?>" class="btn btn-danger">Mandar mensajes</a>
                </div>
            </div>
        </div>
    </div>
</div>