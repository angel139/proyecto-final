<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Managers $model */

$this->title = 'Update Managers: ' . $model->codigoManager;
$this->params['breadcrumbs'][] = ['label' => 'Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoManager, 'url' => ['view', 'codigoManager' => $model->codigoManager]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="managers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
