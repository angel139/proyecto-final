<?php

use app\models\Managers;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Managers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="managers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Manager', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            
            'correoElectronico',
            'nombre',
            'telefono',
            'sueldo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Managers $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoManager' => $model->codigoManager]);
                 }
            ],
        ],
    ]); ?>


</div>
