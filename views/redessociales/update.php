<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Redessociales $model */

$this->title = 'Update Redessociales: ' . $model->codigoRed;
$this->params['breadcrumbs'][] = ['label' => 'Redessociales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoRed, 'url' => ['view', 'codigoRed' => $model->codigoRed]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="redessociales-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
