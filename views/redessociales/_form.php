<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Redessociales $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="redessociales-form">

    <?php $form = ActiveForm::begin(); ?>

   

    <?= $form->field($model, 'plataforma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'seguidores')->textInput() ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigoManager')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Elige un manager'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
