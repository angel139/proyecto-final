<?php

use app\models\Redessociales;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Redes sociales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redessociales-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Redes sociales', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            
            'plataforma',
            'seguidores',
            'direccion',
            'codigoManager',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Redessociales $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoRed' => $model->codigoRed]);
                 }
            ],
        ],
    ]); ?>


</div>
