<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Redessociales $model */

$this->title = $model->codigoRed;
$this->params['breadcrumbs'][] = ['label' => 'Redessociales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="redessociales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoRed' => $model->codigoRed], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoRed' => $model->codigoRed], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoRed',
            'plataforma',
            'seguidores',
            'direccion',
            'codigoManager',
        ],
    ]) ?>

</div>
