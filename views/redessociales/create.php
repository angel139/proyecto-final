<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Redessociales $model */

$this->title = 'Crear Red social';
$this->params['breadcrumbs'][] = ['label' => 'Redes sociales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="redessociales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
