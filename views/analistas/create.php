<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Analistas $model */

$this->title = 'Crear Analistas';
$this->params['breadcrumbs'][] = ['label' => 'Analistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="analistas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
