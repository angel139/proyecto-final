<?php

use app\models\Analistas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Analistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="analistas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Analistas', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            
            'nombre',
            'sueldo',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Analistas $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoAnalista' => $model->codigoAnalista]);
                 }
            ],
        ],
    ]); ?>


</div>
