<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Analistas $model */

$this->title = 'Update Analistas: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Analistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoAnalista, 'url' => ['view', 'codigoAnalista' => $model->codigoAnalista]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="analistas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
