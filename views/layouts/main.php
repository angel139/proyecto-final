<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;



AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/T1_logo.png', ['alt'=>Yii::$app->name, 'class' => 'navbarimage','style' => 'height: 40px']),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md  fixed-top',
            'style' => 'background-color: #000000'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Finanzas', 'url' => ['/finanzas/index']],
            ['label' => 'Estadisticas', 'url' => ['/estadisticas/index'],
            'visible' => $this->context->isAdmin(),],
            
             // Mostrar solo si es administrador
        [
            'label' => 'CRUD',
            'visible' => $this->context->isAdmin(),
            'items' => [
                ['label' => 'Analistas', 'url' => ['/analistas/index']],
                ['label' => 'Campeones', 'url' => ['/campeones/index']],
                ['label' => 'Entrenadores', 'url' => ['/entrenadores/index']],
                ['label' => 'Entrenan', 'url' => ['/entrenan/index']],
                ['label' => 'Juegan', 'url' => ['/juegan/index']],
                ['label' => 'Jugadores', 'url' => ['/jugadores/index']],
                ['label' => 'Patrocinadores', 'url' => ['/patrocinadores/index']],
                ['label' => 'Managers', 'url' => ['/managers/index']],
                ['label' => 'Partidos', 'url' => ['/partidos/index']],
                ['label' => 'Redes sociales', 'url' => ['/redessociales/index']],
                ['label' => 'Teléfonos', 'url' => ['/telefonos/index']],
                ['label' => 'Tienen', 'url' => ['/tienen/index']],
                ['label' => 'Torneos', 'url' => ['/torneos/index']],
                ['label' => 'Usan', 'url' => ['/usan/index']],
            ],
        ],
        ['label' => 'Gestión', 'url' => ['/gestion/index'],
        'visible' => $this->context->isAdmin()],
        
            
        
            ['label' => 'Mensajes', 'url' => ['/mensajes/ver-mensajes'],
            'visible' => !Yii::$app->user->isGuest,],
        
            
            Yii::$app->user->isGuest ? (
                ['label' => 'Iniciar sesión','linkOptions' => ['class' => 'texto-navbar'], 'url' => ['/site/login']]
            ) : (
                
                '<li>'
                
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                . Html::submitButton(
                    'Cerrar sesión (' . Yii::$app->user->identity->nombreUsuario . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; T1Manager <?= date('Y') ?></p>
        
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
