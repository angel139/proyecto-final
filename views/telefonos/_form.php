<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="telefonos-form">

    <?php $form = ActiveForm::begin(); ?>

   <?= $form->field($model, 'nombrePatrocinadores')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Elige un manager'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>


    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
