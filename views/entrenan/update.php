<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Entrenan $model */

$this->title = 'Update Entrenan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Entrenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entrenan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'jugador' => $jugador,
        'entrenador' => $entrenador
    ]) ?>

</div>
