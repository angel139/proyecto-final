<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Entrenan $model */

$this->title = 'Crear Entrenan';
$this->params['breadcrumbs'][] = ['label' => 'Entrenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1
        
    ]) ?>

</div>
