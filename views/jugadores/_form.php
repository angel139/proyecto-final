<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use app\controllers\JugadoresController;

/** @var yii\web\View $this */
/** @var app\models\Jugadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="jugadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombreJuego')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_alta')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_INPUT,
        'value' => date('yyyy-mm-dd', strtotime('+2 days')),
        'options' => ['placeholder' => 'Selecciona una fecha'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'fecha_baja')->widget(DatePicker::classname(), [
        'type' => DatePicker::TYPE_INPUT,
        'value' => date('yyyy-mm-dd', strtotime('+2 days')),
        'options' => ['placeholder' => 'Selecciona una fecha'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'sueldo')->textInput() ?>



    <?= $form->field($model, 'posicion')->widget(Select2::classname(), [
        'data' => ['Superior', 'Jungla', 'Medio', 'ADC', 'Soporte'],
        'options' => ['placeholder' => 'Elige una posición'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>