<?php

/** @var yii\web\View $this */
use yii\helpers\Html;

$this->title = 'Home';
use edofre\fullcalendar\Fullcalendar;
?>

<br>

<h2><i>Bienvenido a T1Manager</i></h2>
<br>
<div id="carouselCiclistas" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
        <li data-target="#carouselCiclistas" data-slide-to="0" class="active"></li>
        <li data-target="#carouselCiclistas" data-slide-to="1" class="active"></li>
        <li data-target="#carouselCiclistas" data-slide-to="2" class="active"></li>
    </ol>
    
    <div class="row justify-content-center">
        <div class=" col-md-12">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?= Html::img('@web/img/slider1.jpg', ['class' => 'item'], ['alt' => 'My logo']) ?>
                </div>
                <div class="carousel-item">
                    <?= Html::img('@web/img/slider2.jpg', ['class' => 'item'], ['alt' => 'My logo']) ?>
                </div>
                <div class="carousel-item">
                    <?= Html::img('@web/img/slider3.jpg', ['class' => 'item'], ['alt' => 'My logo']) ?>
                </div>
            </div>
            <a href="#carouselCiclistas" class="carousel-control-prev" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a href="#carouselCiclistas" class="carousel-control-next" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <h1 class="mt-5" >Eventos</h1>
    <div id="calendario" class="mt-5">
        
         <?php
 \edofre\fullcalendar\Fullcalendar::widget([
    'options' => [
        'id' => 'calendario',
    ],
    'clientOptions' => [
        
        'events' => $eventos,
    ],
]);
        ?>
        
    </div>

        
   


