<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Campeones $model */

$this->title = 'Crear Campeones';
$this->params['breadcrumbs'][] = ['label' => 'Campeones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campeones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data
    ]) ?>

</div>
