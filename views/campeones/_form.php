<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\controllers\CampeonesController;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Campeones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="campeones-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'nombre')->textInput() ?>


    
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
