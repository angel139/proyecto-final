<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Campeones $model */

$this->title = 'Update Campeones: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Campeones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'nombre' => $model->nombre]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="campeones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
