<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Patrocinadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="patrocinadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pagoMensual')->textInput() ?>

    <?= $form->field($model, 'fechaInicio')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('yyyy-mm-dd', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'fechaFinal')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('yyyy-mm-dd', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'correoElectronico')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigoManager')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Elige un manager'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
