<?php
use yii\grid\GridView;
/** @var yii\web\View $this */
use app\controller\EstadisticasController;
use yii\helpers\Html;
$this->title = 'Estadisticas de jugadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'nombreJuego',
        [
            'attribute' => 'indiceVictoria',
            'label' => 'Índice de Victoria',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->indiceVictoria . '%'; // Se concatena el símbolo de porcentaje
            },
        ],
        'totalPartidos',
    ],
]) ?>