<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Entrenadores $model */

$this->title = $model->codigoEntrenador;
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="entrenadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'codigoEntrenador' => $model->codigoEntrenador], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'codigoEntrenador' => $model->codigoEntrenador], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigoEntrenador',
            'sueldo',
            'nombre',
            'mote',
            'fecha_alta',
            'fecha_baja',
            'funcion',
            'codigoAnalista',
        ],
    ]) ?>

</div>
