<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Entrenadores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="entrenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'sueldo')->textInput() ?>

    

    <?= $form->field($model, 'mote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_alta')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('yyyy-mm-dd', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'fecha_baja')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('yyyy-mm-dd', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'dd/mm/yyyy',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'funcion')->widget(Select2::classname(), [
    'data' => ['Entrenador Principal', 'Entrenador Secundario'],
    'options' => ['placeholder' => 'Elige un tipo de entrenador'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

    <?= $form->field($model, 'codigoAnalista')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
