<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Entrenadores $model */

$this->title = 'Update Entrenadores: ' . $model->codigoEntrenador;
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoEntrenador, 'url' => ['view', 'codigoEntrenador' => $model->codigoEntrenador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entrenadores-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
