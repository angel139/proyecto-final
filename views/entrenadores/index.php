<?php

use app\models\Entrenadores;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Entrenadores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            
            'nombre',
            'sueldo',
            'mote',
            'fecha_alta',
            //'fecha_baja',
            //'funcion',
            //'codigoAnalista',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Entrenadores $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigoEntrenador' => $model->codigoEntrenador]);
                 }
            ],
        ],
    ]); ?>


</div>
