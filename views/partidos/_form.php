<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\select2\Select2;
/** @var yii\web\View $this */
/** @var app\models\Partidos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="partidos-form">

    <?php $form = ActiveForm::begin(); ?>
    
    
    
    <?= $form->field($model, 'resultado')->widget(Select2::classname(), [
    'data' => ['Victoria' => 'Victoria', 'Derrota' => 'Derrota'],
    'options' => ['placeholder' => 'Elige un resultado']
]); ?>

   <?= $form->field($model, 'fecha')->widget(DatePicker::classname(),[
        'type' => DatePicker::TYPE_INPUT,
	'value' => date('yyyy-mm-dd', strtotime('+2 days')),
	'options' => ['placeholder' => 'Selecciona una fecha'],
	'pluginOptions' => [
		'format' => 'yyyy-mm-dd',
		'todayHighlight' => true
	]
]);?>

    <?= $form->field($model, 'codigoAnalista')->widget(Select2::classname(), [
    'data' => $data,
    'options' => ['placeholder' => 'Elige un analista'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

    <?= $form->field($model, 'codigoTorneo')->widget(Select2::classname(), [
    'data' => $data1,
    'options' => ['placeholder' => 'Elige un torneo'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
