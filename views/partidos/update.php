<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Partidos $model */

$this->title = 'Update Partidos: ' . $model->codigoPartido;
$this->params['breadcrumbs'][] = ['label' => 'Partidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoPartido, 'url' => ['view', 'codigo' => $model->codigoPartido]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="partidos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'data' => $data,
        'data1' => $data1
    ]) ?>

</div>
