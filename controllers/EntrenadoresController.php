<?php

namespace app\controllers;

use app\models\Entrenadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;

/**
 * EntrenadoresController implements the CRUD actions for Entrenadores model.
 */
class EntrenadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}

    /**
     * Lists all Entrenadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Entrenadores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoEntrenador' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entrenadores model.
     * @param int $codigoEntrenador Código Entrenador
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoEntrenador)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoEntrenador),
        ]);
    }

    /**
     * Creates a new Entrenadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Entrenadores();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoEntrenador' => $model->codigoEntrenador]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Entrenadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoEntrenador Código Entrenador
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoEntrenador)
    {
        $model = $this->findModel($codigoEntrenador);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoEntrenador' => $model->codigoEntrenador]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Entrenadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoEntrenador Código Entrenador
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoEntrenador)
    {
        $this->findModel($codigoEntrenador)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entrenadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoEntrenador Código Entrenador
     * @return Entrenadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoEntrenador)
    {
        if (($model = Entrenadores::findOne(['codigoEntrenador' => $codigoEntrenador])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
