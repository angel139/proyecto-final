<?php

namespace app\controllers;

use app\models\Partidos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use Yii;

/**
 * PartidosController implements the CRUD actions for Partidos model.
 */
class PartidosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}

    /**
     * Lists all Partidos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Partidos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Partidos model.
     * @param string $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoPartido)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoPartido),
        ]);
    }

    /**
     * Creates a new Partidos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $data = ArrayHelper::map(\app\models\Analistas::find()->all(), 'codigoAnalista', 'codigoAnalista');
        $data1 = ArrayHelper::map(\app\models\Torneos::find()->all(), 'codigoTorneo', 'codigoTorneo');
        $model = new Partidos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoPartido' => $model->codigoPartido]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
            'data1' => $data1
        ]);
    }

    /**
     * Updates an existing Partidos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigo Codigo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoPartido)
    {
        $data = ArrayHelper::map(\app\models\Analistas::find()->all(), 'codigoAnalista', 'codigoAnalista');
        $data1 = ArrayHelper::map(\app\models\Torneos::find()->all(), 'codigoTorneo', 'codigoTorneo');
        $model = $this->findModel($codigoPartido);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoPartido' => $model->codigoPartido]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
            'data1' => $data1
        ]);
    }

    /**
     * Deletes an existing Partidos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigo Codigo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo)
    {
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Partidos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigo Codigo
     * @return Partidos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoPartido)
    {
        if (($model = Partidos::findOne(['codigoPartido' => $codigoPartido])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
