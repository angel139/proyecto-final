<?php

namespace app\controllers;

use app\models\Redessociales;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use Yii;
/**
 * RedessocialesController implements the CRUD actions for Redessociales model.
 */
class RedessocialesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}
    /**
     * Lists all Redessociales models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Redessociales::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoRed' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Redessociales model.
     * @param int $codigoRed Codigo Red
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoRed)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoRed),
        ]);
    }

    /**
     * Creates a new Redessociales model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $data = ArrayHelper::map(\app\models\Managers::find()->all(), 'codigoManager', 'codigoManager');
        $model = new Redessociales();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoRed' => $model->codigoRed]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Updates an existing Redessociales model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoRed Codigo Red
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoRed)
    {
        $data = ArrayHelper::map(\app\models\Managers::find()->all(), 'codigoManager', 'codigoManager');
        $model = $this->findModel($codigoRed);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoRed' => $model->codigoRed]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Deletes an existing Redessociales model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoRed Codigo Red
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoRed)
    {
        $this->findModel($codigoRed)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Redessociales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoRed Codigo Red
     * @return Redessociales the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoRed)
    {
        if (($model = Redessociales::findOne(['codigoRed' => $codigoRed])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
