<?php

namespace app\controllers;

use app\models\Entrenan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use Yii;
/**
 * EntrenanController implements the CRUD actions for Entrenan model.
 */
class EntrenanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    protected function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}
    /**
     * Lists all Entrenan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Entrenan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entrenan model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entrenan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */

     
    public function actionCreate()
    {
        $data = ArrayHelper::map(\app\models\Jugadores::find()->all(), 'nombreJuego', 'nombreJuego');
        $data1 = ArrayHelper::map(\app\models\Entrenadores::find()->all(), 'codigoEntrenador', 'codigoEntrenador');
        $model = new Entrenan();
        
        if ($model->load(Yii::$app->request->post())) {
            $codigoEntrenador = Entrenadores::find()
                ->select('codigo')
                ->where(['nombre' => $model->nombre])
                ->scalar();
    
            $model->codigoEntrenador = $codigoEntrenador;
    
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data,
            'data1' => $data1
        ]);
    }}

    /**
     * Updates an existing Entrenan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $data = ArrayHelper::map(\app\models\Analistas::find()->all(), 'codigoAnalista', 'codigoAnalista');
        $data1 = ArrayHelper::map(\app\models\Entrenadores::find()->all(), 'codigoEntrenador', 'codigoEntrenador');
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data,
            'data1' => $data1
        ]);
    }

    /**
     * Deletes an existing Entrenan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entrenan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Entrenan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entrenan::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
