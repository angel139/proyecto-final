<?php
namespace app\controllers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\Jugadores;
use Yii;
class EstadisticasController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find(),
            'pagination' => [
                'pageSize' => 10, 
            ],
        ]);
    
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}
}