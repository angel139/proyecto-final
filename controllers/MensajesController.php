<?php

namespace app\controllers;

use app\models\mensajes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
/**
 * MensajesController implements the CRUD actions for mensajes model.
 */
class MensajesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all mensajes models.
     *
     * @return string
     */

//      public function accessRules()
// {
//     return [
//         [
//             'allow' => true,
//             'actions' => ['view'], // Aquí debe estar 'view' para la acción de ver mensajes
//             'roles' => ['@'], // Esto permite el acceso a cualquier usuario autenticado
//         ],
//         // Otras reglas de acceso aquí para otras acciones si es necesario
//     ];
// }
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => mensajes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single mensajes model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new mensajes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new mensajes();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing mensajes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing mensajes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the mensajes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return mensajes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = mensajes::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}
public function actionVerMensajes()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Mensajes::find()->where(['destinatario' => Yii::$app->user->identity->nombreUsuario])
    ]);

    return $this->render('ver-mensajes', [
        'dataProvider' => $dataProvider,
        
    ]);
}
public function actionMarcarLeido($id)
{
    $mensaje = Mensajes::findOne($id);

    if ($mensaje) {
        

        // Luego, si quieres eliminar el mensaje después de marcarlo como leído
        $mensaje->delete();

        // Redireccionar a la vista de mensajes nuevamente
        return $this->redirect(['ver-mensajes']);
    }

    // Manejo de errores si el mensaje no se encuentra
    // ...

    // Puedes personalizar esta lógica según tus necesidades
}

}
