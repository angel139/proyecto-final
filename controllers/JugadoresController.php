<?php

namespace app\controllers;

use app\models\Jugadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Juegan;
use yii\filters\AccessControl;
use Yii;
/**
 * JugadoresController implements the CRUD actions for Jugadores model.
 */
class JugadoresController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}

    /**
     * Lists all Jugadores models.
     *
     * @return string
     */
    public function actionIndex()
    {
         
        $jugadores = Jugadores::find()->all();
        if (!empty($jugadores)) {
        foreach ($jugadores as $jugador) {
            
            $nombreJuego = $jugador->nombreJuego;

            
            $totalPartidos = Juegan::find()->where(['nombreJuego' => $nombreJuego])->count();

            
            $victorias = Juegan::find()
                ->leftJoin('partidos', 'juegan.codigoPartido = partidos.codigoPartido')
                ->where(['juegan.nombreJuego' => $nombreJuego])
                ->andWhere(['partidos.resultado' => 'victoria'])
                ->count();

           
            $indiceVictoria = $totalPartidos > 0 ? ($victorias / $totalPartidos) * 100 : 0;

            
            $jugador->setAttribute('indiceVictoria', $indiceVictoria);
            $jugador->setAttribute('totalPartidos', $totalPartidos);

            
            $jugador->save();
        }
}
    
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nombreJuego' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'jugador' => isset($jugador) ? $jugador : null,
        ]);
    }

    /**
     * Displays a single Jugadores model.
     * @param string $nombreJuego Nombre Juego
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    
    public function actionEstadisticas()
{
    $jugadores = Jugadores::find()->all();
    
    
    return $this->render('estadisticas', [
        'jugadores' => $jugadores,
    ]);
}
    public function actionView($nombreJuego)
    {
        return $this->render('view', [
            'model' => $this->findModel($nombreJuego),
        ]);
    }

    /**
     * Creates a new Jugadores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Jugadores();
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nombreJuego' => $model->nombreJuego]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Jugadores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nombreJuego Nombre Juego
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nombreJuego)
    {
        $model = $this->findModel($nombreJuego);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nombreJuego' => $model->nombreJuego]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Jugadores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nombreJuego Nombre Juego
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nombreJuego)
    {
        $this->findModel($nombreJuego)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jugadores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nombreJuego Nombre Juego
     * @return Jugadores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nombreJuego)
    {
        if (($model = Jugadores::findOne(['nombreJuego' => $nombreJuego])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
