<?php

namespace app\controllers;

use app\models\Analistas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;

/**
 * AnalistasController implements the CRUD actions for Analistas model.
 */
class AnalistasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}
    /**
     * Lists all Analistas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Analistas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigoAnalista' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Analistas model.
     * @param int $codigoAnalista Codigo Analista
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigoAnalista)
    {
        return $this->render('view', [
            'model' => $this->findModel($codigoAnalista),
        ]);
    }

    /**
     * Creates a new Analistas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Analistas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigoAnalista' => $model->codigoAnalista]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Analistas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigoAnalista Codigo Analista
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigoAnalista)
    {
        $model = $this->findModel($codigoAnalista);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigoAnalista' => $model->codigoAnalista]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Analistas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigoAnalista Codigo Analista
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigoAnalista)
    {
        $this->findModel($codigoAnalista)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Analistas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigoAnalista Codigo Analista
     * @return Analistas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigoAnalista)
    {
        if (($model = Analistas::findOne(['codigoAnalista' => $codigoAnalista])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
