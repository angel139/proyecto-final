<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;

class GestionController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $usuariosCrudUrl = Url::to(['/usuarios/index']);

        // Ruta al CRUD de Mensajes
        $mensajesCrudUrl = Url::to(['/mensajes/index']);
    
        return $this->render('index', [
            'usuariosCrudUrl' => $usuariosCrudUrl,
            'mensajesCrudUrl' => $mensajesCrudUrl,
        ]);
    }
    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}

}