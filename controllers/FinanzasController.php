<?php

namespace app\controllers;
use app\models\Jugadores;
use app\models\Torneos;
use app\models\Entrenadores;
use app\models\Managers;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use Yii;
use yii\data\ArrayDataProvider;
class FinanzasController extends \yii\web\Controller
{
    public function actionIndex()
{
    $jugadores = Jugadores::find()->all();
    $totalSueldo = array_sum(ArrayHelper::getColumn($jugadores, 'sueldo'));

   
    $entrenadores = Entrenadores::find()->all();
    $totalSueldo1 = array_sum(ArrayHelper::getColumn($entrenadores, 'sueldo'));

    $managers = Managers::find()->all();
    $totalSueldo2 = array_sum(ArrayHelper::getColumn($managers, 'sueldo'));

    $total =  -$totalSueldo - $totalSueldo1 - $totalSueldo2;

    $currentDate = date('Y-m-d');

    // Filtra los torneos que han terminado en el mes actual
    $torneosMensuales = Torneos::find()
        ->where(['<=', 'fechaFinal', $currentDate])
        ->andWhere(['>=', 'fechaFinal', date('Y-m-01', strtotime($currentDate))])
        ->all();

    // Calcula las ganancias de los torneos mensuales
    $totalGananciasMensuales = array_sum(ArrayHelper::getColumn($torneosMensuales, 'premioRecibido'));


    $dataProvider = new ArrayDataProvider([
        'allModels' => [
            ['category' => 'Sueldo Jugadores', 'value' => -$totalSueldo],
            ['category' => 'Sueldo Entrenadores', 'value' => -$totalSueldo1],
            ['category' => 'Sueldo Managers', 'value' => -$totalSueldo2],
            ['category' => 'Total', 'value' => $total],
        ],
    ]);

    return $this->render('index', [
        'dataProvider' => $dataProvider,
        'totalGananciasMensuales' => $totalGananciasMensuales,
        'torneosMensuales' => $torneosMensuales,
    ]);
}
    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}

}