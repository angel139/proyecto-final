<?php

namespace app\controllers;

use app\models\Campeones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\Tienen;
use yii\filters\AccessControl;
use Yii;
/**
 * CampeonesController implements the CRUD actions for Campeones model.
 */
class CampeonesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'matchCallback' => function ($rule, $action) {
                            return $this->isAdmin();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function isAdmin()
{
    // Verificar si el usuario actual es administrador
    return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol === 'admin';
}

    /**
     * Lists all Campeones models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $campeones = Campeones::find()->all();

    foreach ($campeones as $campeon) {
    $nombre = $campeon->nombre;

    $asesinatos = Tienen::find()
        ->where(['nombre' => $nombre])
        ->sum('asesinatos');

    $muertes = Tienen::find()
        ->where(['nombre' => $nombre])
        ->sum('muertes');

    $asistencias = Tienen::find()
        ->where(['nombre' => $nombre])
        ->sum('asistencias');

    $campeon->asesinatos = $asesinatos;
    $campeon->muertes = $muertes;
    $campeon->asistencias = $asistencias;

    $campeon->save();
    }
        $dataProvider = new ActiveDataProvider([
            'query' => Campeones::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nombre' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    
}
    /**
     * Displays a single Campeones model.
     * @param string $nombre Nombre
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($nombre)
    {
        return $this->render('view', [
            'model' => $this->findModel($nombre),
        ]);
    }

    /**
     * Creates a new Campeones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $data = ArrayHelper::map(\app\models\Campeones::find()->all(), 'nombre', 'nombre');
        $model = new Campeones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'nombre' => $model->nombre]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Updates an existing Campeones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $nombre Nombre
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($nombre)
    {
        $data = ArrayHelper::map(\app\models\Campeones::find()->all(), 'nombre', 'nombre');
        $model = $this->findModel($nombre);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'nombre' => $model->nombre]);
        }

        return $this->render('update', [
            'model' => $model,
            'data' => $data
        ]);
    }

    /**
     * Deletes an existing Campeones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $nombre Nombre
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($nombre)
    {
        $this->findModel($nombre)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Campeones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $nombre Nombre
     * @return Campeones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($nombre)
    {
        if (($model = Campeones::findOne(['nombre' => $nombre])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function action_Form()
    {
        $nombre = new ActiveDataProvider([
            'query' => Campeones::find()->select('nombre')->all(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'nombre' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('_form', [
            'nombre' => $nombre
        ]);
    }
}
